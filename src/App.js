import React from "react";
import logo from "./logo.svg";

import "./App.css";

class App extends React.Component {
    render() {
        return (
            <div className="app">
                <header className="app-header">
                    <img src={logo} className="app-logo" alt="logo" />
                    <p>React Workshop</p>
                </header>
                {/**
                 *  Insert routes here
                 */}
            </div>
        );
    }
}

export default App;
