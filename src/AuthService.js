import "firebase/auth";
import app from "firebase/app";

class AuthService {
    static register = (email, password) =>
        app.auth().createUserWithEmailAndPassword(email, password);

    static login = (email, password) =>
        app.auth().signInWithEmailAndPassword(email, password);

    static getAuthenticationStatus = (success, error) => {
        app.auth().onAuthStateChanged(user => success(user), err => error(err));
    };
}

export default AuthService;
