## Prerequesities

-   Node.js and NPM installed (https://nodejs.org/en/)
-   Yarn installed globally (_npm install -g yarn_)

## Instructions

1. Clone this project
2. Execute _yarn install_ inside project directory
3. _npm start_ to run the application

## Task

The idea is to create an application where you can add players and randomly generate teams of two for table footbal

-   Login/Root page (base url "/")
    -   form with email and password inputs
    -   submit button
    -   link to _/register_ page
    -   after login redirect to _/players_ page
    -   import AuthService.js and use the login method
-   Register page
    -   form with email and password inputs
    -   submit button
    -   after register redirect to _/_ (root) page
    -   import AuthService.js and use the register method
-   Players page
    -   form for adding players consisted of input and _add_ button
    -   when add button is cilck add the player name to array and clear the input
    -   show the list of players
    -   at the bottom, generate teams button should be shown
    -   use _getFunName_ method from helpers.js to generate team name
    -   use your own logic to choose random players for each team
    -   list the teams

## Example

https://react-workshop-d3e3c.firebaseapp.com
